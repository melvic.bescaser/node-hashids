# node-hashids
Simple hashing/unhashing utility

## Encoding value
```sh
node-hashids encode <value>
```
This will generate a hashed value of given numeric value.
## Decoding value
```sh
node-hashids decode <value>
```
This will generate a raw value of given hashed value.